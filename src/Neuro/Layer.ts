import Neuron from "./Neuron";
interface ILayer {
  neurons: Neuron[];
  input(inputValues: number[]): number[];
  setup(amountInputs: number): void;
  generate(amountInputs: number): Neuron[];
}
class Layer implements ILayer {
  public neurons: Neuron[] = [];
  constructor(private amountNeurons: number) {}
  input(inputValues: number[]): number[] {
    const result = this.neurons.map(neuron => neuron.input(inputValues));
    return result;
  }
  setup(amountInputs: number): void {
    this.neurons = this.generate(amountInputs);
  }
  generate(amountInputs: number): Neuron[] {
    const result: Neuron[] = [];
    for (let i = 0; i <= this.amountNeurons; i++) {
      result.push(new Neuron(amountInputs));
    }
    return result;
  }
}
export default Layer;
