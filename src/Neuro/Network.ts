import Layer from "./Layer";
interface INetwork {
  layers: Layer[];
  amountsInput: number;
  input(inputValues: number[]): number[];
  addLayer(layer: Layer): number;
  validateInput(inputValues: number[]): void;
}
class Network implements INetwork {
  constructor(public amountsInput: number, public layers: Layer[] = []) {}
  //по очереди передает всем слоям и возвращает выход
  public input(inputValues: number[]) {
    this.validateInput(inputValues);
    const result = this.layers.reduce((currentState, layer) => {
      return layer.input(currentState);
    }, inputValues);
    return result;
  }
  public validateInput(inputValues: number[]): void {
    if (this.amountsInput !== inputValues.length) {
      throw new Error("");
    }
  }
  public addLayer(layer: Layer): number {
    return this.layers.push(layer);
  }
}
export default Network;
