import { sigmoid } from "./helpers";
import { zip } from "./helpers";

interface INeuron {
  weights: number[];
  activationFunction: (value: number) => number;
  input(inputValues: number[]): number;
  weighing(inputValues: number[]): number;
}

class Neuron implements INeuron {
  weights: number[];
  public activationFunction: (value: number) => number = sigmoid;
  constructor(amountNodes: number, weights?: number[]) {
    if (weights) {
      this.weights = weights;
    } else {
      this.weights = Array.from({ length: amountNodes }, () => Math.random());
    }
  }
  input(inputValues: number[]): number {
    const weightedValues = this.weighing(inputValues);
    return this.activationFunction(weightedValues);
  }
  // Взвешивание
  public weighing(inputValues: number[]): number {
    return zip(inputValues, this.weights).reduce(
      (acc, [value, weight]) => acc + value * weight,
      0
    );
  }
}

export default Neuron;
