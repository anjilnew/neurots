function sigmoid(value: number): number {
  return 1 / (1 + Math.exp(-value));
}
function zip<T>(...arrays: Array<Array<T>>): Array<Array<T>> {
  return Array(findingTheGreatestLength(arrays))
    .fill(null)
    .map((value, index) => {
      return arrays.map(array => array[index]);
    });
}
function findingTheGreatestLength(arrays: Array<Array<any>>): number {
  return Math.max(...arrays.map(array => array.length));
}
export { sigmoid, zip };
