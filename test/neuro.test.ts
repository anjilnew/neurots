import Network from "../src/Neuro/Network";
import Layer from "../src/Neuro/Layer";
describe("Network", () => {
  const amountInputNodes = 2;
  const amountHiddenNodes = 3;
  const layer = new Layer(amountHiddenNodes);
  const neuro = new Network(5, [layer]);
  test("configuration", () => {
    expect(layer.neurons.length).toBe(amountHiddenNodes);
  });
  test("Direct error propagation", () => {
    const inputValues = [1, 2, 3, 4];
    const expectResult = [2, 2];
    expect(layer.input(inputValues)).toBe(expectResult);
  });
  test("Back propagation error", () => {});
});
